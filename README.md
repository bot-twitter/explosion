# Explosion

Explosion est un bot qui permet de jouer au jeu des explosions en chaîne sur Twitter.

# Installation

Cloner le repository et créer un fichier `credentials.py` à la racine du projet contenant les informations de connexion au compte Twitter :

    KEY                 = "****"
    KEY_SECRET          = "****"
    ACCESS_TOKEN        = "****"
    ACCESS_TOKEN_SECRET = "****"

Pour obtenir ces informations, vous devez d'abord créer un compte développeur sur https://developer.twitter.com/, puis créer une application avec accès en écriture.

# Exécution

Pour lancer le bot, exécutez la commande suivante :

    python3 explosion.py [--cpu max]

L'option `--cpu` permet de définir le nombre maximum de CPU que le bot pourra utiliser. Par défaut, le bot utilisera le nombre de CPU de la machine moins 1.

# Ressources

Le bot va lancer le jeu pour chaque Tweet dans un thread à part. Si tous les CPU sont occupés, les parties seront mises dans une file d'attente. Le fichier GIF généré par le programme peut être assez gros (entre 2.5Mo et 10Mo). Au delà de 5Mo (la limite de Twitter), le programme va automatiquement réduire le nombre de FPS et la taille de l'image finale jusqu'à passer sous la barre des 5Mo. La qualité continuera d'être abaissée si la connexion ne permet pas l'upload des données.
